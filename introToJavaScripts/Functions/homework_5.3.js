// homewrok_5.3:
//     Napisz funkcje, która będzie przyjmować 2 parametry.
//     Funkcja niech sprawdza, czy oba atrybuty są typu number.
//     Funkcja ma zwracać iloczyn (*) obu liczb.
//     Jeżeli któryś z atrybutów nie jest liczba, funkcja niech zwraca false.
//
// Podpowiedź:
// https://developer.mozilla.org/pl/docs/Web/JavaScript/Referencje/Operatory/Operator_typeof
