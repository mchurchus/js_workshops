// homewrok_5.1:
// Napisz funkcje, która przyjmuje 2 parametry:
//     1) imię - np: Ala
//     2) miesiąc - np: styczeń
//
// Funkcja ma zwracać:
//     - jeżeli miesiąc to -> grudzień, styczeń, luty
//     "Ala jeździ na sankach"
//
//     - jeżeli miesiąc to -> marzec, kwiecień, maj
//     "Ala chodzi po kałużach"
//
//     - jeżeli miesiąc to -> czerwiec, lipiec, sierpień
//     "Ala się opala"
//
//     - jeżeli miesiąc to -> wrzesień, październik, listopad "Ala zbiera liście"

// homewrok_5.2:
// Wywołaj funkcje z powyższego zadania przekazując do niej zmienne:
// - Twoje imię
// - dowolny miesiąc
//
// Dopisz w tej funkcji zabezpieczenie, które pozwoli wpisać miesiąc małymi lub dużymi literami.
// Jeżeli miesiąc jest "innym słowem", funkcja niech zwraca "Ala uczy się JS"
//
// Podpowiedź:
// https://developer.mozilla.org/pl/docs/Web/JavaScript/Referencje/Obiekty/String/toLowerCase
// lub
// https://developer.mozilla.org/pl/docs/Web/JavaScript/Referencje/Obiekty/String/toUpperCase


